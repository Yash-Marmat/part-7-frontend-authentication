import React, { Component, Fragment } from "react";
import ReactDOM from "react-dom";
import AlertTemplate from "react-alert-template-basic"; // for the alert box
import Header from "./layout/Header";
import Dashboard from "./leads/Dashboard";
import Alerts from "./layout/Alerts";
import { Provider as AlertProvider } from "react-alert"; // for the alert box
import { Provider } from "react-redux";
import store from "../store";
import { HashRouter as Router, Route, Switch } from "react-router-dom";
import Login from "./accounts/Login";
import Register from "./accounts/Register";
import PrivateRoute from "./common/PrivateRoute";
import { loadUser } from "../actions/auth";

// Alert Options
const alertOptions = {
  timeout: 3000, // 3 seconds
  position: "top center",
};

class App extends Component {
  componentDidMount() {
    store.dispatch(loadUser());
  }

  render() {
    return (
      <div>
        <Provider store={store}>
          <AlertProvider template={AlertTemplate} {...alertOptions}>
            <Router>
              <Fragment>
                <Header />
                <Alerts />
                <div className="container">
                  <Switch>
                    <PrivateRoute exact path="/" component={Dashboard} />
                    <Route exact path="/register" component={Register} />
                    <Route exact path="/login" component={Login} />
                  </Switch>
                </div>
              </Fragment>
            </Router>
          </AlertProvider>
        </Provider>
      </div>
    );
  }
}

ReactDOM.render(<App />, document.getElementById("app"));
