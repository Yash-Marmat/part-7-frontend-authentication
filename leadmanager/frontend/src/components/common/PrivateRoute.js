import React from "react";
import { Route, Redirect } from "react-router-dom";
import { connect } from "react-redux";
import PropTypes from "prop-types";

// here component: Component means PrivateRoute will
// work on all those components where it is declared
// for any other props we will use ...rest
const PrivateRoute = ({ component: Component, auth, ...rest }) => {
  return (
    <div>
      <Route
        {...rest}
        render={(props) => {
          if (auth.isLoading) {
            return <h3>Loading...</h3>;
          } else if (!auth.isAuthenticated) {
            return <Redirect to="/login" />;
          } else {
            return <Component {...props} />;
          }
        }}
      />
    </div>
  );
};

const mapStateToProps = (state) => ({
  auth: state.auth,
});

export default connect(mapStateToProps)(PrivateRoute);
